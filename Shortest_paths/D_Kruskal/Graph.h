

#ifndef TMP_GRAPH_H
#define TMP_GRAPH_H
#include<iostream>
#include<fstream>
#include<vector>
#include<algorithm>

class Graph {
    struct Vertex {
        int parentInd;
        int treeDepth;
    };

    struct Edge {
        int firstVertInd;
        int secondVertInd;
        int weight;
    };

    class DisjointSets {
        std::vector<Vertex> vertices;

    public:
        void init(int numberOfVert);

        void unifyRoots(int firstRootInd, int secondRootInd);

        int findRootInd(int vertInd);
    };


    int numberOfVert;
    std::vector<Edge> edges;

public:
    void init(std::istream& fIn);
    
    int findMinSpanningTreeWeight();
};


#endif //TMP_GRAPH_H
