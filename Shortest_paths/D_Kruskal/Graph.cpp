//
// Created by Semyo_000 on 01.11.2016.
//

#include "Graph.h"

void Graph::DisjointSets::init(int numberOfVert) {
    vertices.resize(numberOfVert + 1);
    for (int i = 1; i < vertices.size(); ++i) {
        vertices[i].parentInd = 0;
        vertices[i]. treeDepth = 1;
    }
}

void Graph::DisjointSets::unifyRoots(int firstRootInd, int secondRootInd) {
    if (vertices[firstRootInd].treeDepth >
        vertices[secondRootInd].treeDepth) {
        vertices[secondRootInd].parentInd = firstRootInd;
    }
    if (vertices[firstRootInd].treeDepth <
        vertices[secondRootInd].treeDepth) {
        vertices[firstRootInd].parentInd = secondRootInd;
    }
    if (vertices[firstRootInd].treeDepth ==
        vertices[secondRootInd].treeDepth) {
        vertices[secondRootInd].parentInd = firstRootInd;
        vertices[firstRootInd].treeDepth++;
    }
}

int Graph::DisjointSets::findRootInd(int vertInd) {
    int rootInd = vertInd;
    while (vertices[rootInd].parentInd != 0) {
        rootInd = vertices[rootInd].parentInd;
    }
    return rootInd;
}

void Graph::init(std::istream &fIn) {
    fIn >> numberOfVert;
    int numberOfEdges;
    fIn >> numberOfEdges;
    edges.resize(numberOfEdges);
    for (int i = 0; i < edges.size(); ++i) {
        fIn >> edges[i].firstVertInd;
        fIn >> edges[i].secondVertInd;
        fIn >> edges[i].weight;
    }
}

int Graph::findMinSpanningTreeWeight() {
    std::sort(edges.begin(), edges.end(),
              [](Edge a, Edge b) {return a.weight < b.weight;});
    int firstRootInd;
    int secondRootInd;
    DisjointSets disjointSets;
    disjointSets.init(numberOfVert);
    int minSpanningTreeWeight = 0;
    for (int i = 0; i < edges.size(); ++i) {
        firstRootInd = disjointSets.findRootInd(edges[i].firstVertInd);
        secondRootInd = disjointSets.findRootInd(edges[i].secondVertInd);
        if (firstRootInd != secondRootInd) {
            minSpanningTreeWeight += edges[i].weight;
            disjointSets.unifyRoots(firstRootInd, secondRootInd);
        }
    }
    return minSpanningTreeWeight;
}