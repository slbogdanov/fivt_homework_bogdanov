#include "Graph.h"

int main(int argc, char* argv[]) {
    std::ifstream fIn;
    fIn.open("kruskal.in");
    Graph graph;
    graph.init(fIn);
    fIn.close();
    std::ofstream fOut;
    fOut.open("kruskal.out");
    fOut << graph.findMinSpanningTreeWeight();
    fOut.close();
    return 0;
}

