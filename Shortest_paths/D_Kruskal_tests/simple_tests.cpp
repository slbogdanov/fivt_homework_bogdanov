#include "gtest/gtest.h"
#include "../D_Kruskal/Graph.h"

TEST(exampleTest, test_eq){
    std::stringstream test_input_stream;
    test_input_stream << "4 4\n1 2 1\n2 3 2\n3 4 5\n4 1 4\n";
    Graph graph;
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMinSpanningTreeWeight(), 7);
}

TEST(fewMoreTests, test_eq){
    std::stringstream test_input_stream;
    Graph graph;
    test_input_stream << "3 3\n1 2 5\n2 3 8\n3 1 29\n";
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMinSpanningTreeWeight(), 13);
    test_input_stream << "5 4\n1 2 1\n2 3 2\n3 4 3\n4 5 4\n";
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMinSpanningTreeWeight(), 10);
    test_input_stream << "4 6\n1 2 1\n2 3 2\n3 4 5\n4 1 4\n1 3 100\n2 4 1";
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMinSpanningTreeWeight(), 4);
}
