#include "gtest/gtest.h"
#include "../C_Floyd/DenseGraph.h"

TEST(exampleTest, test_eq) {
    std::stringstream test_input_stream;
    test_input_stream << "4\n"
            "0 5 9 100\n"
            "100 0 2 8\n"
            "100 100 0 7\n"
            "4 100 100 0";
    DenseGraph<int> denseGraph;
    denseGraph.initialize(test_input_stream);
    std::stringstream test_output_stream;
    denseGraph.findShortestPaths();
    denseGraph.printShortestPaths(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), "0 5 7 13 \n"
            "12 0 2 8 \n"
            "11 16 0 7 \n"
            "4 9 11 0 \n");
    test_input_stream.clear();
    test_output_stream.clear();
}

TEST(fewMoreTests, test_eq) {
    std::stringstream test_input_stream;
    std::stringstream test_output_stream;
    DenseGraph<int> denseGraph;
    test_input_stream << "4\n"
            "0 6 11 23\n"
            "87 0 54 48\n"
            "21 43 0 9\n"
            "4 52 3 0";
    denseGraph.initialize(test_input_stream);
    denseGraph.findShortestPaths();
    denseGraph.printShortestPaths(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), "0 6 11 20 \n"
            "52 0 51 48 \n"
            "13 19 0 9 \n"
            "4 10 3 0 \n");
    test_input_stream.clear();
    std::stringstream test_output_stream1;
    test_input_stream << "3\n"
            "0 61 92\n"
            "10 0 24\n"
            "19 12 0";
    denseGraph.initialize(test_input_stream);
    denseGraph.findShortestPaths();
    denseGraph.printShortestPaths(test_output_stream1);
    EXPECT_EQ(test_output_stream1.str(), "0 61 85 \n"
            "10 0 24 \n"
            "19 12 0 \n");
    test_input_stream.clear();
    std::stringstream test_output_stream2;
    test_input_stream << "4\n"
            "0 51 99 11\n"
            "13 0 22 87\n"
            "89 97 0 77\n"
            "48 13 65 0";
    denseGraph.initialize(test_input_stream);
    denseGraph.findShortestPaths();
    denseGraph.printShortestPaths(test_output_stream2);
    EXPECT_EQ(test_output_stream2.str(), "0 24 46 11 \n"
            "13 0 22 24 \n"
            "89 90 0 77 \n"
            "26 13 35 0 \n");
}

TEST(twoBigTests, test_eq) {
    std::stringstream test_input_stream;
    std::stringstream test_output_stream;
    DenseGraph<int> denseGraph;
    test_input_stream << "15\n"
            "0 41 67 34 0 69 24 78 58 62 64 5 45 81 27 \n"
            "61 0 91 95 42 27 36 91 4 2 53 92 82 21 16 \n"
            "18 95 0 47 26 71 38 69 12 67 99 35 94 3 11 \n"
            "22 33 73 0 64 41 11 53 68 47 44 62 57 37 59 \n"
            "23 41 29 78 0 16 35 90 42 88 6 40 42 64 48 \n"
            "46 5 90 29 70 0 50 6 1 93 48 29 23 84 54 \n"
            "56 40 66 76 31 8 0 44 39 26 23 37 38 18 82 \n"
            "29 41 33 15 39 58 4 0 30 77 6 73 86 21 45 \n"
            "24 72 70 29 77 73 97 12 0 86 90 61 36 55 67 \n"
            "55 74 31 52 50 50 41 24 66 0 30 7 91 7 37 \n"
            "57 87 53 83 45 9 9 58 21 88 0 22 46 6 30 \n"
            "13 68 0 91 62 55 10 59 24 37 48 0 83 95 41 \n"
            "2 50 91 36 74 20 96 21 48 99 68 84 0 81 34 \n"
            "53 99 18 38 0 88 27 67 28 93 48 83 7 0 21 \n"
            "10 17 13 14 9 16 35 51 0 49 19 56 98 3 0 ";
    denseGraph.initialize(test_input_stream);
    denseGraph.findShortestPaths();
    denseGraph.printShortestPaths(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), "0 20 5 30 0 15 15 21 16 22 6 5 15 8 16 \n"
            "18 0 9 30 9 24 19 16 4 2 15 9 16 9 16 \n"
            "12 23 0 25 3 18 18 23 11 25 9 17 10 3 11 \n"
            "22 24 27 0 22 19 11 25 20 26 28 27 36 29 38 \n"
            "21 20 26 36 0 15 15 21 16 22 6 26 19 12 33 \n"
            "23 5 14 21 14 0 10 6 1 7 12 14 21 14 21 \n"
            "27 13 22 29 18 8 0 14 9 15 20 22 25 18 29 \n"
            "21 17 26 15 12 12 4 0 13 19 6 26 19 12 33 \n"
            "24 29 29 27 24 24 16 12 0 31 18 29 31 24 40 \n"
            "16 27 7 32 7 22 17 24 18 0 13 7 14 7 18 \n"
            "15 14 20 30 6 9 9 15 10 16 0 20 13 6 27 \n"
            "12 23 0 25 3 18 10 23 11 25 9 0 10 3 11 \n"
            "2 22 7 32 2 17 17 21 18 24 8 7 0 10 18 \n"
            "9 20 14 35 0 15 15 21 16 22 6 14 7 0 21 \n"
            "10 17 13 14 3 16 16 12 0 19 9 15 10 3 0 \n");
    test_input_stream.clear();
    std::stringstream test_output_stream1;
    test_input_stream << "15\n"
            "0 24 8 44 9 89 2 95 85 93 43 23 87 14 3 \n"
            "48 0 0 58 18 80 96 98 81 89 98 9 57 72 22 \n"
            "38 92 0 38 79 90 57 58 91 15 88 56 11 2 34 \n"
            "72 55 28 0 46 62 86 75 33 69 42 44 16 81 98 \n"
            "22 51 21 99 0 57 76 92 89 75 12 0 10 3 69 \n"
            "61 88 1 89 55 0 23 2 85 82 85 88 26 17 57 \n"
            "32 32 69 54 21 89 0 76 29 68 92 25 55 34 49 \n"
            "41 12 45 60 18 53 39 0 23 79 96 87 29 49 37 \n"
            "66 49 93 95 97 16 86 5 0 88 82 55 34 14 1 \n"
            "16 71 86 63 13 55 85 53 12 0 8 32 45 13 56 \n"
            "21 58 46 82 81 44 96 22 29 61 0 35 50 73 66 \n"
            "44 59 92 39 53 24 54 10 45 49 86 0 13 74 22 \n"
            "68 18 87 5 58 91 2 25 77 14 14 24 0 34 74 \n"
            "72 59 33 70 87 97 18 77 73 70 63 68 92 0 85 \n"
            "2 80 13 27 2 99 27 25 43 24 23 72 61 81 0 ";
    denseGraph.initialize(test_input_stream);
    denseGraph.findShortestPaths();
    denseGraph.printShortestPaths(test_output_stream1);
    EXPECT_EQ(test_output_stream1.str(), "0 24 8 20 5 29 2 15 31 23 17 5 15 8 3 \n"
            "24 0 0 16 18 33 13 19 27 15 23 9 11 2 22 \n"
            "30 29 0 16 28 43 13 32 27 15 23 28 11 2 28 \n"
            "36 34 28 0 36 49 18 38 33 30 30 36 16 30 34 \n"
            "22 22 21 15 0 24 12 10 33 24 12 0 10 3 22 \n"
            "28 14 1 17 20 0 14 2 25 16 24 20 12 3 26 \n"
            "32 32 32 36 21 45 0 31 29 45 33 21 31 24 30 \n"
            "26 12 12 28 18 39 25 0 23 27 30 18 23 14 24 \n"
            "3 17 11 18 3 16 5 5 0 25 15 3 13 6 1 \n"
            "15 29 23 28 13 28 17 17 12 0 8 13 23 13 13 \n"
            "21 34 29 41 26 44 23 22 29 44 0 26 36 29 24 \n"
            "24 22 22 18 24 24 15 10 33 27 27 0 13 24 22 \n"
            "29 18 18 5 23 42 2 25 26 14 14 23 0 20 27 \n"
            "50 50 33 49 39 63 18 49 47 48 51 39 44 0 48 \n"
            "2 24 10 17 2 26 4 12 33 24 14 2 12 5 0 \n");
}