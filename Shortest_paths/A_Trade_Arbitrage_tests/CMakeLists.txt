include_directories(${gtest_SOURCE_DIR}/include S{gtest_SOURCE_DIR})

add_executable(runTradeArbitrageTest simple_tests.cpp)

target_link_libraries(runTradeArbitrageTest gtest gtest_main)
target_link_libraries(runTradeArbitrageTest DenseGraph_A)
