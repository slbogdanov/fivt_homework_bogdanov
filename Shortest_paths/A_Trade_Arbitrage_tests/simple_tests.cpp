#include "gtest/gtest.h"
#include "../A_Trade_Arbitrage/DenseGraph.h"

TEST(exampleTest, test_eq) {
    std::stringstream test_input_stream;
    DenseGraph denseGraph;
    test_input_stream << "2\n"
            "      10.0\n"
            "0.09";
    test_input_stream >> denseGraph;
    EXPECT_EQ(denseGraph.tradeArbitrage(), false);
    test_input_stream.clear();
    test_input_stream << "4\n"
            "      32.1  1.50 78.66\n"
            "0.03        0.04  2.43\n"
            "0.67 21.22       51.89\n"
            "0.01    -1  0.02";
    test_input_stream >> denseGraph;
    EXPECT_EQ(denseGraph.tradeArbitrage(), true);
    test_input_stream.clear();
    test_input_stream << "3\n"
            "     0.67    -1\n"
            "-1        78.66\n"
            "0.02   -1 ";
    test_input_stream >> denseGraph;
    EXPECT_EQ(denseGraph.tradeArbitrage(), true);
}

TEST(fewMoreTests, test_eq) {
    std::stringstream test_input_stream;
    DenseGraph denseGraph;
    test_input_stream << "2\n"
            "      100.0\n"
            "1";
    test_input_stream >> denseGraph;
    EXPECT_EQ(denseGraph.tradeArbitrage(), true);
    test_input_stream.clear();
    test_input_stream << "4\n"
            "     3.5  1.5 81.6\n"
            "0.1       0.9 2.4\n"
            "0.17 17.8     27.3\n"
            "5    -1   0.5";
    test_input_stream >> denseGraph;
    EXPECT_EQ(denseGraph.tradeArbitrage(), true);
    test_input_stream.clear();
    test_input_stream << "3\n"
            "    1   1\n"
            "1       1\n"
            "1   1 ";
    test_input_stream >> denseGraph;
    EXPECT_EQ(denseGraph.tradeArbitrage(), false);
    test_input_stream.clear();
    test_input_stream << "5\n"
            "    1   2   3   4\n"
            "11      43  12  8\n"
            "63  21      72  77\n"
            "27  113 62      51\n"
            "17  34  92  6";
    test_input_stream >> denseGraph;
    EXPECT_EQ(denseGraph.tradeArbitrage(), true);
}