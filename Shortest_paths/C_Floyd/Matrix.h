#ifndef CFLOYD_MATRIX_H
#define CFLOYD_MATRIX_H

#include <vector>
#include <iostream>

template<typename T>
class Matrix;

template<typename T>
std::istream &operator>>(std::istream &input, Matrix<T> &matr);

template<typename T>
std::ostream &operator<<(std::ostream &output, const Matrix<T> &matr);

template<typename T>
class Matrix {
private :
    size_t length;
    size_t height;
    bool isGraph;
    std::vector<std::vector<T> > matrix;
public :
    Matrix();

    Matrix(size_t len, size_t hei);

    size_t getLength() const;

    size_t getHeight() const;

    void setSquare(bool isGr);

    std::vector<T> &operator[](size_t ind);

    const std::vector<T> &operator[](size_t ind) const;

    friend std::istream &operator>><>(std::istream &input, Matrix<T> &matr);

    friend std::ostream &operator<<<>(std::ostream &output,
                                      const Matrix<T> &matr);
};

template<typename T>
Matrix<T>::Matrix() {}

template<typename T>
Matrix<T>::Matrix(size_t len, size_t hei):length(len), height(hei) {
    matrix.resize(height);
    for (size_t i = 0; i < height; ++i) {
        matrix[i].resize(length);
    }
}

template<typename T>
size_t Matrix<T>::getLength() const {
    return length;
}

template<typename T>
size_t Matrix<T>::getHeight() const {
    return height;
}

template<typename T>
void Matrix<T>::setSquare(bool isGr) {
    isGraph = isGr;
}

template<typename T>
std::vector<T> &Matrix<T>::operator[](size_t ind) {
    return matrix[ind];
}

template<typename T>
const std::vector<T> &Matrix<T>::operator[](size_t ind) const {
    return matrix[ind];
}

template<typename T>
std::istream &operator>>(std::istream &input, Matrix<T> &matr) {
    if (matr.isGraph) {
        input >> matr.length;
        matr.height = matr.length;
    } else {
        input >> matr.length;
        input >> matr.height;
    }
    matr.matrix.resize(matr.height);
    for (size_t i = 0; i < matr.height; ++i) {
        matr.matrix[i].resize(matr.length);
        for (size_t j = 0; j < matr.length; ++j) {
            input >> matr.matrix[i][j];
        }
    }
    return input;
}

template<typename T>
std::ostream &operator<<(std::ostream &output, const Matrix<T> &matr) {
    for (size_t i = 0; i < matr.length; ++i) {
        for (size_t j = 0; j < matr.height; ++j) {
            output << matr.matrix[i][j] << " ";
        }
        output << "\n";
    }
    return output;
}


#endif //CFLOYD_MATRIX_H
