#include <fstream>
#include "DenseGraph.h"

int main() {
    DenseGraph<int> graph;
    std::ifstream fIn;
    fIn.open("floyd.in");
    graph.initialize(fIn);
    fIn.close();
    graph.findShortestPaths();
    std::ofstream fOut;
    fOut.open("floyd.out");
    graph.printShortestPaths(fOut);
    fOut.close();
    return 0;
}

