#ifndef CFLOYD_DENSEGRAPH_H
#define CFLOYD_DENSEGRAPH_H

#include "Matrix.h"


template<typename T>
class DenseGraph {
private :
    size_t numberOfVert;
    Matrix<T> adjacencyMatrix;
    Matrix<T> shortestPaths;
public :
    size_t size() const;

    const std::vector<T> &operator[](size_t ind) const;

    void initialize(std::istream &inputStream);

    void findShortestPaths();

    void printShortestPaths(std::ostream &outputStream);
};

template<typename T>
size_t DenseGraph<T>::size() const {
    return numberOfVert;
}

template<typename T>
const std::vector<T> &DenseGraph<T>::operator[](size_t ind) const {
    return adjacencyMatrix[ind];
}

template<typename T>
void DenseGraph<T>::initialize(std::istream &inputStream) {
    adjacencyMatrix.setSquare(true);
    inputStream >> adjacencyMatrix;
    numberOfVert = adjacencyMatrix.getLength();
    shortestPaths = Matrix<T>(numberOfVert, numberOfVert);
}

template<typename T>
void DenseGraph<T>::findShortestPaths() {
    for (size_t i = 0; i < numberOfVert; ++i) {
        for (size_t j = 0; j < numberOfVert; ++j) {
            shortestPaths[i][j] = adjacencyMatrix[i][j];
        }
    }
    for (size_t k = 0; k < numberOfVert; ++k) {
        for (size_t i = 0; i < numberOfVert; ++i) {
            for (size_t j = 0; j < numberOfVert; ++j) {
                if (shortestPaths[i][k] + shortestPaths[k][j] <
                    shortestPaths[i][j]) {
                    shortestPaths[i][j] =
                            shortestPaths[i][k] + shortestPaths[k][j];
                }
            }
        }
    }
}

template<typename T>
void DenseGraph<T>::printShortestPaths(std::ostream &outputStream) {
    outputStream << shortestPaths;
}


#endif //CFLOYD_DENSEGRAPH_H
