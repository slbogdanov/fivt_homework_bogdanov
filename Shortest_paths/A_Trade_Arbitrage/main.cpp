#include "DenseGraph.h"

int main() {
    DenseGraph graph;
    std::cin >> graph;
    if (graph.tradeArbitrage()) {
        std::cout << "YES";
    } else {
        std::cout << "NO";
    }
    return 0;
}