#ifndef TRADEARBITRAGE_DENSEGRAPH_H
#define TRADEARBITRAGE_DENSEGRAPH_H

#include "Matrix.h"


class DenseGraph {
private :
    size_t numberOfVert;
    Matrix adjacencyMatrix;
    std::vector<float> pathLen;

    bool negWeightCycles(size_t start);

public :
    friend std::istream &operator>>(std::istream &input, DenseGraph &graph);

    friend std::ostream &operator<<(std::ostream &output,
                                    const DenseGraph &graph);

    bool tradeArbitrage();
};


#endif //TRADEARBITRAGE_DENSEGRAPH_H
