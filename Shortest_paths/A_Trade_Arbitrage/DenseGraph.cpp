#include "DenseGraph.h"

bool DenseGraph::negWeightCycles(size_t start) {
    for (size_t i = 0; i < numberOfVert; ++i) {
        if (i == start) {
            pathLen[i] = 1;
        } else {
            pathLen[i] = -1;
        }
    }
    for (size_t k = 0; k < numberOfVert / 2 + 1; ++k) {
        for (size_t i = 0; i < numberOfVert; ++i) {
            for (size_t j = i + 1; j < numberOfVert; ++j) {
                if (pathLen[j] > -1 && adjacencyMatrix[j][i] > -1 &&
                    (pathLen[i] == -1 ||
                     pathLen[j] * adjacencyMatrix[j][i] > pathLen[i])) {
                    pathLen[i] = pathLen[j] * adjacencyMatrix[j][i];
                }
            }
        }
        for (size_t i = 0; i < numberOfVert; ++i) {
            for (size_t j = 0; j < i; ++j) {
                if (pathLen[j] > -1 && adjacencyMatrix[j][i] > -1 &&
                    (pathLen[i] == -1 ||
                     pathLen[j] * adjacencyMatrix[j][i] > pathLen[i])) {
                    pathLen[i] = pathLen[j] * adjacencyMatrix[j][i];
                }
            }
        }
    }
    for (size_t i = 0; i < numberOfVert; ++i) {
        for (size_t j = 0; j < numberOfVert; ++j) {
            if (pathLen[j] > -1 && adjacencyMatrix[j][i] > -1 &&
                (pathLen[i] == -1 || pathLen[j] *
                                     adjacencyMatrix[j][i] > pathLen[i])) {
                return true;
            }
        }
    }
    return false;
}

bool DenseGraph::tradeArbitrage() {
    pathLen.resize(numberOfVert);
    for (size_t i = 0; i < numberOfVert; ++i) {
        if (negWeightCycles(i)) {
            return true;
        }
    }
    return false;
}

std::istream &operator>>(std::istream &input, DenseGraph &graph) {
    graph.adjacencyMatrix.setSquare(true);
    input >> graph.adjacencyMatrix;
    graph.numberOfVert = graph.adjacencyMatrix.getLength();
    return input;
}

std::ostream &operator<<(std::ostream &output, const DenseGraph &graph) {
    output << graph.adjacencyMatrix;
    return output;
}
