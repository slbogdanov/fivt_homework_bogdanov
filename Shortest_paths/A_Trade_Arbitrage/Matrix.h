#ifndef TRADEARBITRAGE_MATRIX_H
#define TRADEARBITRAGE_MATRIX_H

#include <vector>
#include <iostream>


class Matrix {
private :
    size_t length;
    size_t height;
    bool isUndirGraph;
    std::vector<std::vector<float> > matrix;

public :
    Matrix();

    Matrix(size_t len, size_t hei);

    size_t getLength() const;

    size_t getHeight() const;

    void setSquare(bool isUndGr);

    std::vector<float> &operator[](size_t ind);

    const std::vector<float> &operator[](size_t ind) const;

    friend std::istream &operator>>(std::istream &input, Matrix &matr);

    friend std::ostream &operator<<(std::ostream &output,
                                    const Matrix &matr);
};


#endif //TRADEARBITRAGE_MATRIX_H
