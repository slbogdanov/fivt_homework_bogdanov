#include "Matrix.h"

Matrix::Matrix() {}

Matrix::Matrix(size_t len, size_t hei) : length(len), height(hei) {
    matrix.resize(height);
    for (size_t i = 0; i < height; ++i) {
        matrix[i].resize(length);
    }
}

size_t Matrix::getLength() const {
    return length;
}

size_t Matrix::getHeight() const {
    return height;
}

void Matrix::setSquare(bool isUndGr) {
    isUndirGraph = isUndGr;
}

std::vector<float> &Matrix::operator[](size_t ind) {
    return matrix[ind];
}

const std::vector<float> &Matrix::operator[](size_t ind) const {
    return matrix[ind];
}

std::istream &operator>>(std::istream &input, Matrix &matr) {
    if (matr.isUndirGraph) {
        input >> matr.length;
        matr.height = matr.length;
    } else {
        input >> matr.length;
        input >> matr.height;
    }
    matr.matrix.resize(matr.height);
    for (size_t i = 0; i < matr.height; ++i) {
        matr.matrix[i].resize(matr.length);
        for (size_t j = 0; j < matr.length; ++j) {
            if (i == j && matr.isUndirGraph) {
                matr.matrix[i][j] = 0;
            } else {
                input >> matr.matrix[i][j];
            }
        }
    }
    return input;
}

std::ostream &operator<<(std::ostream &output, const Matrix &matr) {
    for (size_t i = 0; i < matr.length; ++i) {
        for (size_t j = 0; j < matr.height; ++j) {
            output << matr.matrix[i][j] << " ";
        }
        output << "\n";
    }
    return output;
}