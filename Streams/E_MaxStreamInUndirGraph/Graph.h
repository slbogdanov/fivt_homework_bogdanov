#ifndef PROJECT_GRAPH_H
#define PROJECT_GRAPH_H

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>


class Graph {
    struct Vertex {
        int excessiveFlow;
        int height;
        std::list<int> neighbours;
        std::list<int>::iterator neighboursIt;
    };

    int numberOfVert;
    int startIndex;
    int finishIndex;
    std::vector<std::vector<int>> capacityNetwork;
    std::vector<std::vector<int>> preflow;
    std::vector<Vertex> vertices;
    std::list<int> vertList;
    std::list<int>::iterator vertListIt;

    bool input(std::istream &fIn);

    void cleanUp();

    void inputEdges(std::istream &fIn);

    void push(int firstVId, int secondVId);

    bool isPushPossible(int firstVId, int secondVId);

    void relabel(int vertInd);

    void discharge(int vertInd);

    bool isVertFull(int vertInd);

    int calcMaxFlow();

public:
    bool init(std::istream &fIn);

    int findMaxFlow();
};


#endif //PROJECT_GRAPH_H
