#include "Graph.h"
#include <fstream>

int main() {
    Graph graph;
    std::ifstream fIn;
    fIn.open("input.txt");
    std::ofstream fOut;
    fOut.open("output.txt");
    while (graph.init(fIn)) {
        fOut << graph.findMaxFlow() << "\n";
    }
    fIn.close();
    fOut.close();
    return 0;
}
