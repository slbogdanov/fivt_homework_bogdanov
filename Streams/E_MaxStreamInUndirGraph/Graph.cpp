#include "Graph.h"

bool Graph::input(std::istream &fIn) {
    cleanUp();
    fIn >> numberOfVert;
    if (!numberOfVert) {
        return false;
    }
    fIn >> startIndex >> finishIndex;
    ++numberOfVert;
    vertices.resize(numberOfVert);
    capacityNetwork.resize(numberOfVert);
    preflow.resize(numberOfVert);
    for (int i = 1; i < numberOfVert; ++i) {
        capacityNetwork[i].resize(numberOfVert);
        preflow[i].resize(numberOfVert);
    }
    inputEdges(fIn);
    return true;
}

void Graph::cleanUp() {
    if (!capacityNetwork.empty()) {
        for (int i = 1; i < capacityNetwork.size(); ++i) {
            capacityNetwork[i].clear();
        }
        capacityNetwork.clear();
    }
    if (!preflow.empty()) {
        for (int i = 1; i < preflow.size(); ++i) {
            preflow[i].clear();
        }
        preflow.clear();
    }
    if (!vertices.empty()) {
        vertices.clear();
    }
    if (!vertList.empty()) {
        vertList.erase(vertList.begin(), vertList.end());
    }
}

void Graph::inputEdges(std::istream &fIn) {
    int firstVertInd;
    int secondVertInd;
    int capacity;
    int numberOfEdges;
    fIn >> numberOfEdges;
    for (int i = 0; i < numberOfEdges; ++i) {
        fIn >> firstVertInd >> secondVertInd;
        fIn >> capacity;
        capacityNetwork[firstVertInd][secondVertInd] += capacity;
        capacityNetwork[secondVertInd][firstVertInd] += capacity;
    }
}

void Graph::push(int firstVId, int secondVId) {
    int capacity = capacityNetwork[firstVId][secondVId];
    int posFlow = capacity - preflow[firstVId][secondVId];
    int excessiveFlow = vertices[firstVId].excessiveFlow;
    int flow = excessiveFlow < posFlow ? excessiveFlow : posFlow;
    preflow[firstVId][secondVId] += flow;
    preflow[secondVId][firstVId] -= flow;
    vertices[firstVId].excessiveFlow -= flow;
    vertices[secondVId].excessiveFlow += flow;
}

bool Graph::isPushPossible(int firstVId, int secondVId) {
    if (isVertFull(firstVId) &&
        capacityNetwork[firstVId][secondVId] -
        preflow[firstVId][secondVId] > 0 &&
        vertices[firstVId].height > vertices[secondVId].height) {
        return true;
    } else {
        return false;
    }
}

void Graph::relabel(int vertInd) {
    int minHeight = numberOfVert * 2;
    for (int i = 1; i < numberOfVert; ++i) {
        if (vertices[i].height < minHeight &&
            (capacityNetwork[vertInd][i] - preflow[vertInd][i]) > 0) {
            minHeight = vertices[i].height;
        }
    }
    vertices[vertInd].height = minHeight + 1;
}

void Graph::discharge(int vertInd) {
    while (isVertFull(vertInd)) {
        if (vertices[vertInd].neighboursIt ==
            vertices[vertInd].neighbours.end()) {
            relabel(vertInd);
            vertices[vertInd].neighboursIt =
                    vertices[vertInd].neighbours.begin();
        } else {
            if (isPushPossible(vertInd,
                               *(vertices[vertInd].neighboursIt))) {
                push(vertInd, *(vertices[vertInd].neighboursIt));
            } else {
                ++(vertices[vertInd].neighboursIt);
            }
        }
    }
}

bool Graph::isVertFull(int vertInd) {
    if (vertInd != startIndex && vertInd != finishIndex &&
        vertices[vertInd].excessiveFlow > 0) {
        return true;
    } else {
        return false;
    }
}

int Graph::calcMaxFlow() {
    int maxFlow = 0;
    for (int i = 1; i < numberOfVert; ++i) {
        maxFlow += preflow[i][finishIndex];
    }
    return maxFlow;
}

bool Graph::init(std::istream &fIn) {
    if (!input(fIn)) {
        return false;
    }
    std::vector<int> tmpVect;
    for (int i = 1; i < numberOfVert; ++i) {
        for (int j = 1; j < numberOfVert; ++j) {
            if (capacityNetwork[i][j] > 0) {
                vertices[i].neighbours.push_front(j);
            }
        }
        vertices[i].neighboursIt = vertices[i].neighbours.begin();
        vertices[i].height = 0;
        vertices[i].excessiveFlow = 0;
        if (i != startIndex) {
            preflow[startIndex][i] = capacityNetwork[startIndex][i];
            preflow[i][startIndex] = capacityNetwork[startIndex][i] * -1;
            vertices[i].excessiveFlow = capacityNetwork[startIndex][i];
        }
        if (i != startIndex && i != finishIndex) {
            tmpVect.push_back(i);
        }
    }
    vertices[startIndex].height = numberOfVert;
    std::random_shuffle(tmpVect.begin(), tmpVect.end());
    for (int i = 0; i < tmpVect.size(); ++i) {
        vertList.push_front(tmpVect[i]);
    }
    vertListIt = vertList.begin();
    return true;
}

int Graph::findMaxFlow() {
    while (vertListIt != vertList.end()) {
        int prevHeight = vertices[*vertListIt].height;
        discharge(*vertListIt);
        if (prevHeight != vertices[*vertListIt].height) {
            int vert = *vertListIt;
            vertList.erase(vertListIt);
            vertList.push_front(vert);
            vertListIt = vertList.begin();
        }
        ++vertListIt;
    }
    return calcMaxFlow();
}