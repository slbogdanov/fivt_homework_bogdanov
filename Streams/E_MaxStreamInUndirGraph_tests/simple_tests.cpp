#include "gtest/gtest.h"
#include "../E_MaxStreamInUndirGraph/Graph.h"

TEST(exampleTest, test_eq) {
    std::stringstream test_input_stream;
    test_input_stream << "4\n"
            "1 4 5\n"
            "1 2 20\n"
            "1 3 10\n"
            "2 3 5\n"
            "2 4 10\n"
            "3 4 20\n"
            "0";
    Graph graph;
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMaxFlow(), 25);
}

TEST(fewMoreTests, test_eq) {
    std::stringstream test_input_stream;
    Graph graph;
    test_input_stream << "4\n"
            "1 4 6\n"
            "1 2 4\n"
            "1 3 0\n"
            "1 4 4\n"
            "2 3 4\n"
            "2 4 5\n"
            "3 4 5\n"
            "0";
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMaxFlow(), 8);
    test_input_stream << "3\n"
            "1 3 3\n"
            "1 2 7\n"
            "1 3 4\n"
            "2 3 0\n"
            "0";
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMaxFlow(), 4);
    test_input_stream << "4\n"
            "1 4 6\n"
            "1 2 3\n"
            "1 3 9\n"
            "1 4 5\n"
            "2 3 6\n"
            "2 4 9\n"
            "3 4 0\n"
            "0";
    graph.init(test_input_stream);
    EXPECT_EQ(graph.findMaxFlow(), 14);
}