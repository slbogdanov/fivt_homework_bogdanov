#include "gtest/gtest.h"
#include "../G_HammingDistance/HammingDistanceCalculator.h"

TEST(exampleTest, test_eq) {
    std::stringstream test_input_stream;
    test_input_stream << "00?\n"
            "1?";
    HammingDistanceCalculator calculator;
    calculator.inputFromStream(test_input_stream);
    std::stringstream test_output_stream;
    test_output_stream << calculator.calculateHammingDistance() << "\n";
    calculator.printResultStrings(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), std::string("2\n"
                                                            "000\n"
                                                            "10"));
}

TEST(firstSimpleTest, test_eq) {
    std::stringstream test_input_stream;
    HammingDistanceCalculator calculator;
    std::stringstream test_output_stream_1;
    test_input_stream << "?110\n"
            "0?";
    calculator.inputFromStream(test_input_stream);
    test_output_stream_1 << calculator.calculateHammingDistance() << "\n";
    calculator.printResultStrings(test_output_stream_1);
    EXPECT_EQ(test_output_stream_1.str(), std::string("3\n"
                                                              "0110\n"
                                                              "01"));
}

TEST(secondSimpleTest, test_eq) {
    std::stringstream test_input_stream;
    HammingDistanceCalculator calculator;
    std::stringstream test_output_stream;
    test_input_stream << "01?01\n"
            "1????";
    calculator.inputFromStream(test_input_stream);
    test_output_stream << calculator.calculateHammingDistance() << "\n";
    calculator.printResultStrings(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), std::string("1\n"
                                                            "01001\n"
                                                            "11001"));
}

TEST(thirdSimpleTest, test_eq) {
    std::stringstream test_input_stream;
    HammingDistanceCalculator calculator;
    std::stringstream test_output_stream;
    test_input_stream << "1?1\n"
            "0";
    calculator.inputFromStream(test_input_stream);
    test_output_stream << calculator.calculateHammingDistance() << "\n";
    calculator.printResultStrings(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), std::string("2\n"
                                                            "101\n"
                                                            "0"));
}

TEST(firstGeneratedTest, test_eq) {
    std::stringstream test_input_stream;
    HammingDistanceCalculator calculator;
    std::stringstream test_output_stream;
    test_input_stream << "?1100?0?01?011????001?10010?100?1?011?1?0\n"
            "1000?110?10?????11?100??111";
    calculator.inputFromStream(test_input_stream);
    test_output_stream << calculator.calculateHammingDistance() << "\n";
    calculator.printResultStrings(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), std::string("170\n"
                                                            "11100000010011000000101001011001110111110\n"
                                                            "100001100100000011010011111"));
}

TEST(secondGeneratedTest, test_eq) {
    std::stringstream test_input_stream;
    HammingDistanceCalculator calculator;
    std::stringstream test_output_stream;
    test_input_stream
            << "?1100?0?01?011????001?10010?100?1?011?1?01000?110?10?????11?100??1111?01010010?0110?011???100100??0010?0?11?0?0100?100??????01110?1???11011?0?10?111??1?11100?0????11?1?1010?101100????0001010?1?101?1?01?0110000??01111?01?0011000???1010?010?00??0111?0101101?1???0?111??0?0?11?11????????01?00?00000??100?00??0?01?0?110?0?00?10?0?1?101?0??100?0???1110001??1?0??1?10?11?0?000010?10??1110?10??1??1?01011???10??0?100?10?01101?0111??01111?100?001?0101?11011?0001?0?10?11001001?00000?10?01?1?111?010?01????110?1?11?111?10?011101\n"
                    "??0011???10000???1?11?001?101000?1?1?010011?000??00001110?1?0111??11?1??010?0?0001?1?11?100000??110???1?0?1?11?11111111?000101?";
    calculator.inputFromStream(test_input_stream);
    test_output_stream << calculator.calculateHammingDistance() << "\n";
    calculator.printResultStrings(test_output_stream);
    EXPECT_EQ(test_output_stream.str(), std::string("21895\n"
                                                            "111001010110111111001110010110011101111101000111011011111111100111111101010010101101011111100100110010101111010100110011111101110111111101110110111111111110010111111111101011011001111000101011110111101101100001101111101100110001111010101010011011110101101111110111111010111111111111110110010000011100100110101101110101001101011110110111001011111100011111011111011110100001011011111011011111110101111110110110011010110110111110111111001001101011110111000110110111001001100000110101111111101010111111101111111111100011101\n"
                                                            "1100111111000011111111001110100011111010011100011000011101110111111111110101010001111111100000111101111101111111111111110001011"));
}




