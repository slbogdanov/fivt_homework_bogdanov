#ifndef PROJECT_GRAPH_H
#define PROJECT_GRAPH_H

#include <iostream>
#include <string>
#include <vector>
#include <queue>


class Graph {
    struct Vertex {
        int height;
        std::vector<int> neighbours;
        int firstNotCheckedEdge;
    };
    int numberOfVert;
    int startIndex;
    int finishIndex;
    std::vector<std::vector<int>> capacityNetwork;
    std::vector<std::vector<int>> residualNetwork;
    std::vector<Vertex> vertices;

    void init();

    void setHeights();

    int getAndPushStreamInPath(int curVertInd, int curFlow);

    int findBlockingStreamInLayeredNetwork();

    bool buildLayeredNetworkAndFindBlockingStream(int &newStream);

public:
    void setSizes(int numOfVert);

    void setStartAndFinish(int startInd, int finishInd);

    void setCapacity(int firstVertInd, int secondVertInd, int capacity);

    int findMaxStream();

    void getReachableVert(std::vector<bool> &reachableVert);
};


#endif //PROJECT_GRAPH_H
