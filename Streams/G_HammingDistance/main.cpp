#include "HammingDistanceCalculator.h"
#include <fstream>

int main() {
    std::ifstream fIn;
    fIn.open("input.txt");
    std::ofstream fOut;
    fOut.open("output.txt");
    HammingDistanceCalculator calculator;
    calculator.inputFromStream(fIn);
    fOut << calculator.calculateHammingDistance() << "\n";
    calculator.printResultStrings(fOut);
    fIn.close();
    fOut.close();
    return 0;
}

