#ifndef PROJECT_HAMMINGDISTANCECALCULATOR_H
#define PROJECT_HAMMINGDISTANCECALCULATOR_H

#include "Graph.h"


class HammingDistanceCalculator {
    Graph graph;
    std::string givenString;
    std::string givenTemplate;
    std::vector<int> posOfSpacesInStr;
    std::vector<int> posOfSpacesInTempl;

    int startDist();

    int distForSpaceInTemp(int spacePos, char symb);

    int distForSpaceInStr(int spacePos, char symb);

    void setGraph();

public:
    void inputFromStream(std::istream &fIn);

    int calculateHammingDistance();

    void printResultStrings(std::ostream &fOut);
};


#endif //PROJECT_HAMMINGDISTANCECALCULATOR_H
