#include "Graph.h"

void Graph::init() {
    for (int i = 1; i < numberOfVert; ++i) {
        for (int j = 1; j < numberOfVert; ++j) {
            if (capacityNetwork[i][j] > 0) {
                vertices[i].neighbours.push_back(j);
            }
        }
    }
    residualNetwork = capacityNetwork;
}

void Graph::setHeights() {
    std::queue<int> queueToCalcHeight;
    queueToCalcHeight.push(startIndex);
    int queueTop;
    while (!queueToCalcHeight.empty()) {
        queueTop = queueToCalcHeight.front();
        for (int i = 0; i < vertices[queueTop].neighbours.size(); ++i) {
            int neightbour = vertices[queueTop].neighbours[i];
            if (vertices[neightbour].height == vertices.size() &&
                residualNetwork[queueTop][neightbour] > 0) {
                vertices[neightbour].height = vertices[queueTop].height + 1;
                queueToCalcHeight.push(neightbour);
            }
        }
        queueToCalcHeight.pop();
    }
}

int Graph::getAndPushStreamInPath(int curVertInd, int curFlow) {
    if (curFlow == 0) {
        return 0;
    }
    if (curVertInd == finishIndex) {
        return curFlow;
    }
    int pushed;
    for (int i = vertices[curVertInd].firstNotCheckedEdge;
         i < vertices[curVertInd].neighbours.size(); ++i) {
        int neighbour = vertices[curVertInd].neighbours[i];
        ++(vertices[curVertInd].firstNotCheckedEdge);
        if (((vertices[curVertInd].height + 1) ==
             vertices[neighbour].height) &&
            residualNetwork[curVertInd][neighbour] > 0) {
            if (curFlow == -1) {
                pushed = getAndPushStreamInPath(neighbour,
                                                residualNetwork[curVertInd][neighbour]);
            } else {
                pushed = getAndPushStreamInPath(neighbour,
                                                std::min(curFlow, residualNetwork[curVertInd][neighbour]));
            }
            if (pushed) {
                residualNetwork[curVertInd][neighbour] -= pushed;
                residualNetwork[neighbour][curVertInd] += pushed;
                return pushed;
            }
        }
    }
    return 0;
}

int Graph::findBlockingStreamInLayeredNetwork() {
    int newStream = 0;
    int pushed;
    do {
        pushed = getAndPushStreamInPath(startIndex, -1);
        newStream += pushed;
    } while (pushed > 0);
    return newStream;
}

bool Graph::buildLayeredNetworkAndFindBlockingStream(int &newStream) {
    for (int i = 0; i < vertices.size(); ++i) {
        vertices[i].height = vertices.size();
        vertices[i].firstNotCheckedEdge = 0;
    }
    vertices[startIndex].height = 0;
    setHeights();
    if (vertices[finishIndex].height == vertices.size()) {
        return false;
    } else {
        newStream = findBlockingStreamInLayeredNetwork();
        return true;
    }
}

void Graph::setSizes(int numOfVert) {
    numberOfVert = numOfVert;
    ++numberOfVert;
    vertices.resize(numberOfVert);
    capacityNetwork.resize(numberOfVert);
    residualNetwork.resize(numberOfVert);
    for (int i = 1; i < numberOfVert; ++i) {
        capacityNetwork[i].resize(numberOfVert);
        residualNetwork[i].resize(numberOfVert);
    }
}

void Graph::setStartAndFinish(int startInd, int finishInd) {
    startIndex = startInd;
    finishIndex = finishInd;
}

void Graph::setCapacity(int firstVertInd, int secondVertInd, int capacity) {
    capacityNetwork[firstVertInd][secondVertInd] += capacity;
    capacityNetwork[secondVertInd][firstVertInd] += capacity;
}

int Graph::findMaxStream() {
    int curStream = 0;
    int newStream;
    init();
    while (buildLayeredNetworkAndFindBlockingStream(newStream)) {
        curStream += newStream;
    }
    return curStream;
}

void Graph::getReachableVert(std::vector<bool> &reachableVert) {
    reachableVert.resize(numberOfVert);
    std::queue<int> indQueue;
    indQueue.push(startIndex);
    int curInd;
    while (!indQueue.empty()) {
        curInd = indQueue.front();
        for (int i = 1; i < numberOfVert; ++i) {
            if (residualNetwork[curInd][i] > 0 && !reachableVert[i]) {
                reachableVert[i] = true;
                indQueue.push(i);
            }
        }
        indQueue.pop();
    }
}