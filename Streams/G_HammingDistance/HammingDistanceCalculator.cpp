#include "HammingDistanceCalculator.h"

int HammingDistanceCalculator::startDist() {
    int dist = 0;
    for (int j = 0; j < givenTemplate.size(); ++j) {
        for (int i = j; i < givenString.size() &&
                        givenTemplate.size() + i <=
                        givenString.size() + j; ++i) {
            if ((givenString[i] == '1' && givenTemplate[j] == '0') ||
                (givenString[i] == '0' && givenTemplate[j] == '1')) {
                ++dist;
            }
        }
    }
    return dist;
}

int HammingDistanceCalculator::distForSpaceInTemp(int spacePos, char symb) {
    int dist = 0;
    for (int i = spacePos; i + givenTemplate.size() <=
                           givenString.size() + spacePos; ++i) {
        if (givenString[i] == symb) {
            ++dist;
        }
    }
    return dist;
}

int HammingDistanceCalculator::distForSpaceInStr(int spacePos, char symb) {
    int dist = 0;
    int startCount = spacePos + givenTemplate.size() < givenString.size() ?
                     0 : spacePos + givenTemplate.size() - givenString.size();
    for (int i = startCount; i <= spacePos &&
                             i < givenTemplate.size(); ++i) {
        if (givenTemplate[i] == symb) {
            ++dist;
        }
    }
    return dist;
}

void HammingDistanceCalculator::setGraph() {
    for (int i = 0; i < givenString.size(); ++i) {
        if (givenString[i] == '?') {
            posOfSpacesInStr.push_back(i);
        }
    }
    for (int i = 0; i < givenTemplate.size(); ++i) {
        if (givenTemplate[i] == '?') {
            posOfSpacesInTempl.push_back(i);
        }
    }
    int numberOfVert = posOfSpacesInStr.size() +
                       posOfSpacesInTempl.size() + 2;
    graph.setSizes(numberOfVert);
    graph.setStartAndFinish(1, numberOfVert);
    for (int i = 2; i < numberOfVert; ++i) {
        if (i < posOfSpacesInTempl.size() + 2) {
            graph.setCapacity(1, i,
                              distForSpaceInTemp(posOfSpacesInTempl[i - 2], '1'));
            graph.setCapacity(i, numberOfVert,
                              distForSpaceInTemp(posOfSpacesInTempl[i - 2], '0'));
        } else {
            graph.setCapacity(1, i,
                              distForSpaceInStr(posOfSpacesInStr[i - 2 - posOfSpacesInTempl.size()], '1'));
            graph.setCapacity(i, numberOfVert,
                              distForSpaceInStr(posOfSpacesInStr[i - 2 - posOfSpacesInTempl.size()], '0'));
        }
    }
    for (int i = 0; i < posOfSpacesInTempl.size(); ++i) {
        for (int j = 0; j < posOfSpacesInStr.size(); ++j) {
            if (posOfSpacesInTempl[i] <= posOfSpacesInStr[j] &&
                posOfSpacesInStr[j] + givenTemplate.length() <=
                posOfSpacesInTempl[i] + givenString.length()) {
                graph.setCapacity(2 + i,
                                  2 + posOfSpacesInTempl.size() + j, 1);
            }
        }
    }
}

void HammingDistanceCalculator::inputFromStream(std::istream &fIn) {
    fIn >> givenString >> givenTemplate;
}

int HammingDistanceCalculator::calculateHammingDistance() {
    setGraph();
    return graph.findMaxStream() + startDist();
}

void HammingDistanceCalculator::printResultStrings(std::ostream &fOut) {
    std::vector<bool> reachableVert;
    graph.getReachableVert(reachableVert);
    int curInd = 0;
    while (curInd < posOfSpacesInTempl.size()) {
        givenTemplate[posOfSpacesInTempl[curInd]] =
                reachableVert[curInd + 2] ? '1' : '0';
        ++curInd;
    }
    while (curInd < posOfSpacesInTempl.size() + posOfSpacesInStr.size()) {
        givenString[posOfSpacesInStr[curInd - posOfSpacesInTempl.size()]] =
                reachableVert[curInd + 2] ? '1' : '0';
        ++curInd;
    }
    fOut << givenString << "\n" << givenTemplate;
}
